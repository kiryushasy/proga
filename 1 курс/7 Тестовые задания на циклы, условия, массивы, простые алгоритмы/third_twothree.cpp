#include <iostream>
#include <string>
#include <cmath>
using namespace std;

int main() {
  int num, base = 3;
  string newNum1 = "", newNum2 = "";
  cin >> num;
  while (num > 0) {
    newNum1 = to_string(num % base) + newNum1;
    num /= base;
  }
  for (int i = 1; i <= newNum1.length(); i++){
    newNum2 += newNum1.substr(i-1, 1) + newNum1.substr(i-1, 1);
  }
  int newNum3 = 0;
  for (int i = 1; i <= newNum2.length(); i++){
    newNum3 += stoi(newNum2.substr(i-1, 1)) * pow(3, newNum2.length() - i);
  }
  cout << newNum3;
  return(0);
}
