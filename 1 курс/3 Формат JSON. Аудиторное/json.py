import json

info = {
  "user1": {
    "time": "2021-5-20-20-16",
    "duration": 7855
  },
  "user2": {
    "time": "2021-5-28-16-20",
    "duration": 7855
  },
  "user3": {
    "time": "2021-5-23-14-20",
    "duration": 7855
  },
  "user4": {
    "time": "2021-4-15-13-20",
    "duration": 7855
  },
  "user5": {
    "time": "2021-5-27-20-16",
    "duration": 7855
  }
}

with open("info.json", "w", encoding = "utf-8") as outfile:
    json.dump(info, outfile, indent = 2)