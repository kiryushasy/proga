import heapq
from math import log

def is_complete(H):
  arr = list(map(int, H.split()))
  heapq.heapify(arr)
  if (log(len(H)+1, 2) % 1 == 0):
      return True
  else:
      return False

  return True

H = input ("Введите кучу через пробелы: ")

print(is_complete(H))