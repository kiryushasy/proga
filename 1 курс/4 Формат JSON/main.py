import json
import requests

def user_name():
  user_info = open("user_info.json", "w")
  user_data = {
    "user": {
      "name": "user1",
      "time": "2-3-15",
      "duration": "11.09.2001"
    }
  }
  json.dump(user_data, user_info)
  user_info.close()
  return user_data 

def inf():
  appId = "035a49d9903d2259525fbd4b39ab4680"
  city = "Irkutsk"
  URL = "http://api.openweathermap.org/data/2.5/weather?q="
  req = requests.get(URL+ city + "&appid=" + appId)
  return req

def user_output(obj):
    if "user" in obj:
      print("name: ", obj["user"]["name"])
      print("time: ", obj["user"]["time"])
      print("duration: ", obj["user"]["duration"])
      print()
    else:
      print("Температура:", int(obj["main"]["temp"] - 273), "°C")
      print("Скорость ветра:", int(obj["wind"]["speed"])), "м/с"
      print("Влажность:", str(obj["main"]["humidity"]) + "%")
      print("Ощущается как:", int(obj["main"]["feels_like"] - 273),"°C")
      

info = inf().json()
user_name()
user_output(info)
with open ("json.json", 'w', encoding ="utf-8") as file1:
  json.dump(info, file1, indent = 2) 
