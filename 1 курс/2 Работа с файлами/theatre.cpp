#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <list>

using namespace std;

int main() {
  setlocale(LC_ALL, "Russian");

  multimap<string, list<string>> replicas;
  list<string> roles;
  string stroka;

  ifstream InputFile;
  InputFile.open("roles.txt");
  
  while(getline(InputFile, stroka)){
   if (stroka != "roles:"){
     if (stroka != "textLines:"){
       stroka = stroka + ":";
       replicas.insert(make_pair(stroka, roles));
      }
     else{
       break;
     }
   }
  }

  multimap<string, list<string>>::iterator it = replicas.begin();
  multimap<string, list<string>>::iterator it3 = replicas.begin();
  int i = 0;
  int position = -1;
  int count = 0;
  string role;

  while(getline(InputFile, stroka)){
    count = 0;
    for(it = replicas.begin(); it != replicas.end(); it++){
      position = stroka.find(it->first);
      if(position != -1){
        i++;
        count = 1;
        stroka = to_string(i) + ")"+ stroka.substr(it->first.size(), stroka.size());
        it->second.push_back(stroka);
        it3 = replicas.find(it->first);
      }
    }
    if(count == 0){
      it3->second.push_back(stroka);
    }
  }

  multimap<string, list<string>>::iterator it1;
  list<string>:: iterator it2 = it->second.begin();

  ofstream openfile;
  openfile.open("result.txt");
  for (it1 = replicas.begin(); it1 != replicas.end(); it1++){
    openfile << it1->first << endl;
    for (it2 = it1->second.begin(); it2 != it1->second.end(); it2++){
      openfile << " " << *it2 << '\n';
    }
  }
  
  openfile.close();
  InputFile.close();  
}